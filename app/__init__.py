from flask import Flask

app = Flask(__name__)
app.config['SECRET_KEY'] = 'thesecretkey'

from app import views
from app import model
