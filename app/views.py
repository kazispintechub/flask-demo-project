from app import app
from flask import render_template,request,redirect,make_response
from app.model import db, Student, Course, Enrollments

@app.route('/')
def index():
    students = Student.query.all()
    if not students:
        print("No student found. Add the students now!")
    else:
        print(students)
    return render_template('index.html',students = students)

@app.route('/student/create', methods = ["POST","GET"])
def add_students():
    if request.method == 'POST':
        roll_number = request.form.get("roll_number")
        first_name = request.form.get("first_name")
        last_name = request.form.get("last_name")
        checkbox = request.form.getlist('courses')
        # print(roll_number, first_name, last_name)
        data = Student(
                    roll_number = roll_number,
                    first_name = first_name,
                    last_name = last_name
                    )
        db.session.add(data)
        db.session.commit()
        res = Student.query.filter_by(roll_number=roll_number).first()
        for i in checkbox:
            course_id = i.split('_')
            course = Enrollments(
            estudent_id = res.student_id, ecourse_id = course_id[1])
            db.session.add(course)
            db.session.commit()
        return render_template('index.html')
    return render_template('form.html')

@app.route('/student/<int:id>',  methods = ["GET"])
def student_info(id):
    data = Student.query.filter_by(student_id = id).first()
    result = Enrollments.query.filter_by(estudent_id = id).all()
    courses = []
    for r in result:
        course = Course.query.filter_by(course_id = r.ecourse_id)
        courses.append(course)
    for c in courses:
        print('courses', c)
    # print(courses[0].course_name)
    return "hello"

@app.route('/student/<int:id>/update', methods = ["POST","GET"])
def update_students(id):
    data = Student.query.filter_by(student_id = id).first()
    result= Enrollments.query.filter_by(estudent_id = id).all()
    sub = []
    for r in result:
        sub.append(r.ecourse_id)
    if request.method == 'POST':
        print(data, data.roll_number, data.first_name)

        roll_number = request.get("roll_number")
        first_name = request.get("first_name")
        last_name = request.get("last_name")
        print(roll_number, first_name)
        checkbox = request.form.getlist('courses')
        data.roll_number = roll_number
        data.first_name = first_name
        data.last_name = last_name
        db.session.commit()
        if result:
            db.session.delete(result)
            db.session.commit()
        for i in checkbox:
            for i in checkbox:
                course_id = i.split('_')
                course = Enrollments(estudent_id = res.student_id, ecourse_id = course_id[1])
                db.session.add(course)
                db.session.commit()
        return render_template('index.html')
    return render_template('form.html', student = data, course = sub)
